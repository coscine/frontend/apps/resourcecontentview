declare type CoscineType = {
  i18n: {
    resourcecontentview: VueI18n.LocaleMessages | undefined;
  };
};

declare global {
  const coscine: CoscineType;
}

declare const _spPageContextInfo: unknown;

declare interface Window {
  coscine: CoscineType;
}

export default interface ResourceContentSettings {
  readOnly: boolean;
  metadataView: {
    editableDataUrl: boolean;
    editableKey: boolean;
  };
  entriesView: {
    columns: {
      always: Array<string>;
    };
  };
}
