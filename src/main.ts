import jQuery from "jquery";
import BootstrapVue from "bootstrap-vue";
import "bootstrap-vue/dist/bootstrap-vue.css";
import Vue from "vue";
import ResourceContentView from "./ResourceContentView.vue";
import VueI18n from "vue-i18n";
import { LanguageUtil } from "@coscine/app-util";
import VueRouter from "vue-router";

import Vuex from "vuex";
import store from "@coscine/vuex-store";

Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(VueI18n);
Vue.use(VueRouter);
Vue.use(Vuex);

jQuery(() => {
  const i18n = new VueI18n({
    locale: LanguageUtil.getLanguage(),
    messages: (window as any).coscine.i18n.resourcecontentview,
    silentFallbackWarn: true,
  });

  new Vue({
    store,
    render: (h) => h(ResourceContentView),
    i18n,
  }).$mount("resourcecontentview");
});
