declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}

declare module "@coscine/api-connection";
declare module "@coscine/app-util";
declare module "@coscine/vuex-store";
declare module "@coscine/form-generator";
declare module "@coscine/component-library";

declare module "vue-loading-overlay";

declare module "*.png" {
  const value: string;
  export default value;
}
