import ResourceContentSettings from "@/global";
export class DefaultResourceContentSettings implements ResourceContentSettings {
  public readOnly = false;
  public metadataView = {
    editableDataUrl: false,
    editableKey: false,
  };
  public entriesView = {
    columns: {
      always: [],
    },
  };
}
