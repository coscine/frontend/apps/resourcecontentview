export interface EntryDefinition {
  path: string;
  version?: string;
  uploading?: boolean;
  name: string;
  metadata?: Record<string, any[]>;
  isFolder: boolean;
  dataUrl?: string;
  absolutepath: string;
  lastModified?: string;
  created?: string;
  size?: number;
  requesting?: boolean;
  info?: File;
  showOnly?: boolean;
  id?: string;
}
