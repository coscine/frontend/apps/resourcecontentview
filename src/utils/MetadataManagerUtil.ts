import { TreeApi } from "@coscine/api-client";
import { EntryDefinition } from "./EntryDefinition";

export default {
  filterMetadataStorage(
    metadataStorage: Record<string, any>[],
    filterPath: string
  ): Record<string, any> {
    return metadataStorage.filter((x) => {
      if (Object.keys(x).length > 0) {
        const graphName = Object.keys(x)[0];
        const pathQueryString = "path=";
        if (graphName.indexOf(pathQueryString) !== -1) {
          let path = graphName.substr(
            graphName.indexOf(pathQueryString) + pathQueryString.length
          );
          if (path.indexOf("&") !== -1) {
            path = path.substr(0, path.indexOf("&"));
          }
          if (
            decodeURIComponent(path) === filterPath ||
            decodeURIComponent(path) === "/" + filterPath
          ) {
            return true;
          }
        }
      }
      return false;
    });
  },
  async loadMetadata(
    callback: (response: any) => void,
    fileInfo: EntryDefinition,
    resourceId: string
  ) {
    if (fileInfo !== undefined && fileInfo.absolutepath !== undefined) {
      const response = await TreeApi.treeGetMetadataWithParameter(
        resourceId,
        fileInfo.absolutepath
      );

      const metadataStorage = response.data.data.metadataStorage;

      if (metadataStorage.length === 0) {
        return callback({});
      }

      const resultArray = this.filterMetadataStorage(
        metadataStorage,
        fileInfo.absolutepath
      );

      if (resultArray.length === 0) {
        return callback({});
      }

      const result = resultArray[0];

      const objectKeys = Object.keys(result);
      if (
        response.data.data.metadataStorage !== undefined &&
        objectKeys.length === 1
      ) {
        return callback(result[objectKeys[0]]);
      } else if (
        response.data.data.metadataStorage !== undefined &&
        objectKeys.length > 1
      ) {
        return callback(result);
      }
    } else {
      return callback({});
    }
  },
  copyMetadata(
    source: any,
    target: any,
    copyStructure = true,
    createNode = false
  ) {
    let copyTarget = target;
    if (createNode) {
      target.metadata = {};
      copyTarget = target.metadata;
    }
    const objectKeys = Object.keys(source);
    for (const objectKey of objectKeys) {
      if (copyStructure) {
        copyTarget[objectKey] = [];
      }
      for (let i = 0; i < source[objectKey].length; i++) {
        if (copyStructure) {
          copyTarget[objectKey].push({});
        }
        const innerObjectKeys = Object.keys(source[objectKey][i]);
        for (const innerObjectKey of innerObjectKeys) {
          if (copyStructure) {
            copyTarget[objectKey][i][innerObjectKey] =
              source[objectKey][i][innerObjectKey];
          } else if (innerObjectKey === "value") {
            copyTarget[objectKey] = source[objectKey][i][innerObjectKey];
          }
        }
      }
    }
  },
};
